from flask import render_template, flash, redirect, url_for, request
from app import app, db
from app.formularz import LoginForm, WprowadzForm, RegistrationForm
from flask_login import current_user, login_user, logout_user, login_required
from app.modele import User, Tekst
from werkzeug.urls import url_parse
from app.formularz import RegistrationForm


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('rejestracja.html', title='Register', form=form)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = WprowadzForm()
    posts = Tekst.query.all()
    if form.validate_on_submit():
        tekst=Tekst(body=form.tekst.data, name=current_user.username)
        db.session.add(tekst)
        db.session.commit()
        posts = Tekst.query.all()
        #flash(tekst.body)
        #flash(tekst.name)


       # flash('Dziala') #
        return redirect(url_for('index'))
    return render_template('index.html', title='Tekst', posts=posts, form=form)


@app.route('/logowanie', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('logowanie.html', title='Sign In', form=form)


app.run()